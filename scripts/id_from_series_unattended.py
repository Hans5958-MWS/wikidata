#!/usr/bin/python3
"""
Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always		   The bot won't ask for confirmation when putting a page

-summary:		 Set the action summary message for the edit.

Every script has its own section with the script name as header.

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
from typing import Iterator
import pywikibot
from pywikibot import pagegenerators
import pywikibot.bot
import lib.id_from_series

DEFAULT_ARGS = {
	'minor': True,
	'force': False,
	'error-limit': 32
}

class IdFromSeriesUnattendedBot(pywikibot.CurrentPageBot, pywikibot.bot.SingleSiteBot):

	"""
	Infer reference IDs of TV series, seasons, and episods from vertically adjacent levels. (unattended mode, for Toolforge)
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	# def __init__(self, generator: Iterator, **args):
		
	# 	self.generator = generator

	# 	self.site = pywikibot.Site("wikidata", "wikidata")
	# 	self.repo = self.site.data_repository()

	# 	print(self.site.username())

	# 	for item in generator:
	# 		self.treat_item(item)

	error_count = 0

	def treat_page(self):
		# return lib.id_from_series.main(self, self.current_page)
		try:
			lib.id_from_series.main(self, self.current_page)
			self.error_count = 0
		except Exception as e:
			print("Error occured! Skipping gracefully.")
			print(e)
			self.error_count += 1
			if self.error_count == self.update_options['error-limit']:
				print(f"{self.update_options['error-limit']} errors in the row have been occured. Quitting!")
				raise e

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen = pagegenerators.WikidataSPARQLPageGenerator("""
	SELECT DISTINCT (?series AS ?item) WHERE {  
		?episode p:P31 ?instanceOf.
		?instanceOf (ps:P31/(wdt:P279*)) wd:Q21191270.

		MINUS {
			?episode wdt:P8013 ?episodeTraktId.
			?episode wdt:P1712 ?episodeMetacriticId.
		}

		?episode wdt:P179 ?series.  
	}
	""")

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary', 'text'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	bot = IdFromSeriesUnattendedBot(generator=gen, **args)
	bot.run()  #

	

if __name__ == '__main__':
	main()
