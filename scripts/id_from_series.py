#!/usr/bin/python3
"""
Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always		   The bot won't ask for confirmation when putting a page

-summary:		 Set the action summary message for the edit.

Every script has its own section with the script name as header.

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
from typing import Iterator
import pywikibot
from pywikibot import pagegenerators
import pywikibot.bot
import lib.id_from_series

DEFAULT_ARGS = {
	'minor': True,
	'force': False
}

class IdFromSeriesBot(pywikibot.WikidataBot, pywikibot.CurrentPageBot, pywikibot.bot.SingleSiteBot):

	"""
	Infer reference IDs of TV series, seasons, and episods from vertically adjacent levels.

	"""

	update_options = DEFAULT_ARGS

	def treat_page(self):
		item_page = pywikibot.ItemPage(self.repo, self.current_page.title())
		lib.id_from_series.main(self, item_page)

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary', 'text'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = IdFromSeriesBot(generator=gen, **args)
		bot.run()	

if __name__ == '__main__':
	main()
