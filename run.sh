echo "This is for reading only! Do not run directly!"
exit 0

# Fresh start? Get Pywikibot on the pwb folder and run these below.
pwb generate_family_file
pwb generate_user_files

# Login to user account
pwb login

# ----

# Running Pywikibot scripts
python -m scripts.id_from_series_unattended
python -m scripts.id_from_series -page:Q4507 -log

# Running on Toolforge
cd ~/wikidata
git pull
cd ~

toolforge jobs run bootstrap-venv --command "cd ~/wikidata && ../bootstrap_venv.sh" --image python3.11 --wait
chmod +x ~/wikidata/toolforge/id_from_series_unattended.sh
toolforge jobs run wikidata.id-from-series --command "cd ~/wikidata && ./toolforge/id_from_series_unattended.sh" --image python3.11 --emails all
toolforge jobs list
