from lib.id_from_series.classes import EpisodeMatch, ItemMatch, SeasonMatch, ReferenceIDMeta
from lib.shared import generate_summary
from lib.id_from_series.trakt import TraktMeta
from lib.id_from_series.metacritic import MetacriticMeta
from lib.id_from_series.utils import get_parts, get_ref_from_item, pretty_label, generate_title_ref, generate_infer_ref, generate_direct_url_ref
import pywikibot
import copy

ref_id = [
    TraktMeta, 
    MetacriticMeta,
]

TASK_ID = 1
TEST_ID = ""    # 8 characters please

def main(self: pywikibot.CurrentPageBot, series: pywikibot.ItemPage):

    site = self.site
    repo = site.data_repository()

    for ref_id_meta in ref_id:

        pywikibot.info(f"Running for {ref_id_meta.name}.")

        series_match, seasons, episodes = fetch_initial_items(ref_id_meta, series)

        if not series_match.reference_id:
            pywikibot.info("No reference ID of series can be found. Skipping!")
            continue

        infill_missing_data(series_match, seasons, episodes)

        update_data_on_repo(repo, ref_id_meta, series_match, seasons, episodes)

def fetch_initial_items(ref_id_meta, series):

    pywikibot.info("Fetching initial items...")

    series_ref_id = get_ref_from_item(series, ref_id_meta)
    series_ref_id_disp = ''

    if series_ref_id:
        series_ref_id = ref_id_meta.ref_id_class(series_ref_id)
        series_ref_id_disp = str(series_ref_id)
        pywikibot.info(f"{pretty_label(series)} -> {series_ref_id_disp}")
    else:
        pywikibot.info(f"{pretty_label(series)}")

    if series_ref_id and not series_ref_id.is_valid_format():
        series_ref_id = None

    seasons = {}
    episodes = {}
    series_match = ItemMatch(series, series_ref_id)
    season_items = get_parts(series)

    for season_ordinal, season in list(season_items.items()):

        if not season:
            continue

        # if season_ordinal > 1:
        #     break

        season_ref_id = get_ref_from_item(season, ref_id_meta)
        season_ref_id_disp = ''

        if season_ref_id:
            season_ref_id = ref_id_meta.ref_id_class(season_ref_id)
            season_ref_id_disp = str(season_ref_id).replace(str(series_ref_id), '...')
            pywikibot.info(f"├── {season_ordinal}. {pretty_label(season)} -> {season_ref_id_disp}")
        else:
            pywikibot.info(f"├── {season_ordinal}. {pretty_label(season)}")

        if season_ref_id and not season_ref_id.is_valid_format():
            season_ref_id = None

        season_match = SeasonMatch(season, season_ref_id)

        episode_items = get_parts(season)
        for episode_ordinal, episode in list(episode_items.items()):

            if not episode:
                continue
            
            # if episode_ordinal > 1:
            #     break

            # episode: pywikibot.ItemPage

            episode_ref_id = get_ref_from_item(episode, ref_id_meta)
            if episode_ref_id:
                episode_ref_id = ref_id_meta.ref_id_class(episode_ref_id)
                episode_ref_id_disp = str(episode_ref_id).replace(str(season_ref_id), '...')
                pywikibot.info(f"    ├── {episode_ordinal}. {pretty_label(episode)} -> {episode_ref_id_disp}")
            else:
                pywikibot.info(f"    ├── {episode_ordinal}. {pretty_label(episode)}")
                

            if episode_ref_id and not episode_ref_id.is_valid_format():
                episode_ref_id = None

            if episode_ref_id and not season_ref_id:
                pywikibot.info("Inferring season ID based on episode ID")
                season_ref_id = copy.deepcopy(episode_ref_id)
                season_ref_id.episode = None
                season_ref_id.episode_on_ref = None
                season_match.inferred_from = episode

            episodes[(season_ordinal, episode_ordinal)] = EpisodeMatch(episode, episode_ref_id)

        if season_ref_id and not series_ref_id:
            pywikibot.info("Inferring series ID based on season ID")
            series_ref_id = copy.deepcopy(season_ref_id)
            series_ref_id.season = None
            series_ref_id.season_on_ref = None
            series_match.inferred_from = season_ref_id

        season_match.episode_count = len(episode_items)
        seasons[season_ordinal] = season_match

    return series_match, seasons, episodes

def infill_missing_data(series_match: ItemMatch, seasons, episodes):

    pywikibot.info("Infilling missing data...")

    series_ref_id = series_match.reference_id

    # Build season ID from series
    for season_ordinal, season_match in seasons.items():
        # season_match: SeasonMatch

        if season_match.reference_id:
            continue

        season_ref_id = copy.deepcopy(series_ref_id)
        season_ref_id.season = season_ordinal
        season_match.reference_id = season_ref_id
        season_match.inferred_from = series_match.item
        pywikibot.info(f"{season_ordinal}. Setting reference ID to {season_ref_id}")

    # Build episode ID from season
    for (season_ordinal, episode_ordinal), episode_match in episodes.items():
        # episode_match: EpisodeMatch
        season_match: SeasonMatch = seasons[season_ordinal]

        if episode_match.reference_id:
            season_match.episode_match_count += 1
            continue

        if not seasons[season_ordinal].reference_id:
            continue

        episode_ref_id = copy.deepcopy(season_match.reference_id)
        episode_ref_id.episode = episode_ordinal
        episode_ref_id.post_infill()

        if season_match.reference_id_orig:
            # Safe to assume it has seasons set before
            episode_match.inferred_from = season_match.item
        else:
            episode_match.inferred_from = series_match.item

        is_valid = episode_ref_id.is_valid_external(episode_match.item)
        if is_valid:
            pywikibot.info(f"{season_ordinal}x{episode_ordinal}. Setting reference ID to {episode_ref_id}")
            episode_match.reference_id = episode_ref_id
            season_match.episode_match_count += 1

    # If 95% of episodes has good episode ID, just apply the episode back
    # for (season_ordinal, episode_ordinal), episode_match in episodes.items():
    #     # episode_match: EpisodeMatch
    #     season_match = seasons[season_ordinal]
    #
    #     if season_match.episode_match_count == season_match.episode_count or season_match.episode_match_count / season_match.episode_count < 0.95:
    #         continue
    #
    #     if episode_ref_id or not seasons[season_ordinal][1]:
    #         continue
    #
    #     episode_ref_id = copy.deepcopy(season_match.reference_id)
    #     episode_ref_id.episode = episode_ordinal
    #     episode_match.reference_id = episode_ref_id
    #     episode_match.is_matched_by_parent_threshold = True
    #
    #     pywikibot.info(
    #         f"{season_ordinal}x{episode_ordinal}. Setting reference ID to {episode_ref_id} for above threshold on a season")

    for season_ordinal, season_match in seasons.items():
        # season_match: SeasonMatch

        if season_match.episode_match_count > 0:
            continue

        season_match.reference_id = None
        pywikibot.info(f"{season_ordinal}. Removing ID due to no valid episodes")

def update_data_on_repo(repo, ref_id_meta, series_match: ItemMatch, seasons, episodes):

    pywikibot.info("Updating data on Wikibase repository (Wikidata)...")

    current_ts = pywikibot.Timestamp.now()
    today_target = pywikibot.WbTime.fromTimestamp(current_ts, 11)
    today_target.second = today_target.minute = today_target.hour = 0

    set_claim_to_series_match(series_match, ref_id_meta, repo, today_target)

    for season_ordinal, season_match in seasons.items():
        set_claim_to_season_match(season_ordinal, season_match, ref_id_meta, repo, today_target)

    for episode_tuple_ordinal, episode_match in episodes.items():
        set_claim_to_episode_match(episode_tuple_ordinal, episode_match, ref_id_meta, repo, today_target)

def set_claim_to_series_match(series_match: ItemMatch, ref_id_meta, repo, today_target):

    summary = generate_summary(1, f"Infer {ref_id_meta.name} of TV series from its descendants", TEST_ID, False if TEST_ID else True)
    
    item = series_match.item
    reference_id = series_match.reference_id

    if not reference_id:
        return
    
    prefix_log = '0'

    item_dict = item.get()
    if ref_id_meta.property_id in item_dict['claims']:
        pywikibot.info(f"{prefix_log}. Skipped adding claim since it already has one")
        return

    pywikibot.info(f"{prefix_log}. Adding claim")

    ref_id_claim = pywikibot.Claim(repo, ref_id_meta.property_id)
    ref_id_claim.setTarget(str(reference_id))
    
    claim_sources_data = {
        "inferred_from": series_match.inferred_from
    }
    
    for sources in reference_id.generate_claim_sources(claim_sources_data, today_target, repo):
        ref_id_claim.addSources(sources, summary=summary)

    item.addClaim(ref_id_claim, summary=summary)
    # pywikibot.info(trakt_id_claim)

def set_claim_to_season_match(season_ordinal, season_match: SeasonMatch, ref_id_meta, repo, today_target):

    summary = generate_summary(1, f"Infer {ref_id_meta.name} of TV series season from its series", TEST_ID, False if TEST_ID else True)
    
    if season_match.reference_id == season_match.reference_id_orig:
        return
    
    item = season_match.item
    reference_id = season_match.reference_id

    if not reference_id:
        return

    prefix_log = season_ordinal

    item_dict = item.get()
    if ref_id_meta.property_id in item_dict['claims']:
        pywikibot.info(f"{prefix_log}. Skipped adding claim since it already has one")
        return

    pywikibot.info(f"{prefix_log}. Adding claim")

    ref_id_claim = pywikibot.Claim(repo, ref_id_meta.property_id)
    ref_id_claim.setTarget(str(reference_id))

    claim_sources_data = {
        "inferred_from": season_match.inferred_from
    }
    
    for sources in reference_id.generate_claim_sources(claim_sources_data, today_target, repo):
        ref_id_claim.addSources(sources, summary=summary)

    item.addClaim(ref_id_claim, summary=summary)
    # pywikibot.info(trakt_id_claim)

def set_claim_to_episode_match(episode_tuple_ordinal, episode_match: EpisodeMatch, ref_id_meta: ReferenceIDMeta, repo, today_target):

    summary = generate_summary(1, f"Infer {ref_id_meta.name} of TV series episodes from its season", TEST_ID, False if TEST_ID else True)
    
    if episode_match.reference_id == episode_match.reference_id_orig:
        return
    
    item = episode_match.item
    reference_id = episode_match.reference_id
    is_matched_by_title = ref_id_meta.use_title_compare or episode_match.is_matched_by_title
    
    if not reference_id:
        return

    prefix_log = f"{episode_tuple_ordinal[0]}x{episode_tuple_ordinal[1]}"

    item_dict = item.get()
    if ref_id_meta.property_id in item_dict['claims']:
        pywikibot.info(f"{prefix_log}. Skipped adding claim since it already has one")
        return

    pywikibot.info(f"{prefix_log}. Adding claim")

    ref_id_claim = pywikibot.Claim(repo, ref_id_meta.property_id)
    ref_id_claim.setTarget(str(reference_id))

    claim_sources_data = {
        "inferred_from": episode_match.inferred_from
    }

    for sources in reference_id.generate_claim_sources(claim_sources_data, today_target, repo):
        ref_id_claim.addSources(sources, summary=summary)
    
    item.addClaim(ref_id_claim, summary=summary)
    # pywikibot.info(trakt_id_claim)
