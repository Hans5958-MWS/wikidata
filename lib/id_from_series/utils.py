import re
import pywikibot
from difflib import SequenceMatcher

from lib.id_from_series.classes import ReferenceID, ReferenceIDMeta

def pretty_label(item: pywikibot.ItemPage):
    """
    Builds a pretty label.
    """
    item_dict = item.get()
    return f"{item.getID()} {item_dict['labels']['en'] if 'en' in item_dict['labels'] else '[no label in English]'}".strip()

def lax_title(title):
    title = str(title)
    title = title.replace('v.', 'vs')
    title = ''.join(title.split())
    title = title.lower()
    title = re.sub('[^a-z0-9]', '', title)
    return title

def lax_title_comparator(a: str, b: str):
    return a == b or lax_title(a) == lax_title(b) or SequenceMatcher(None, a, b).ratio() > 0.8 or SequenceMatcher(None, lax_title(a), lax_title(b)).ratio() > 0.8

def at_least_one_title_match(a: list, b: list):
    for a_entry in a:
        for b_entry in b:
            if a_entry == b_entry:
                return True
            if lax_title_comparator(a_entry, b_entry):
                return True
    return False

def check_item_class_in_list(item: pywikibot.ItemPage, instance_of_list: list[str]):
    item_dict = item.get()
    claims = item_dict['claims']['P31'] # instance of
    for claim in claims:
        instance_of = claim.target.getID()
        if instance_of in instance_of_list:
            return True
    return False

def check_if_item_is_series(item: pywikibot.ItemPage):
    check_item_class_in_list(item, SERIES_CLASSES)

def get_parts(item: pywikibot.ItemPage) -> dict[int, pywikibot.ItemPage]:
    parts = {}
    item_dict = item.get()
    # I don't get why `'P257' not in item_dict['claims']` didn't work.
    try:
        claims = item_dict['claims']['P527']    # has part(s)
    except KeyError:
        return parts

    for claim in claims:
        claim: pywikibot.Claim
        if not 'P1545' in claim.qualifiers:     # series ordinal
            print('No series ordinal. Skipping!')
            continue
        if len(claim.qualifiers['P1545']) != 1:
            print("Multiple series ordinal found. Skipping!")
            continue
        series_ordinal_qualifier_claim = claim.qualifiers['P1545'][0]
        series_ordinal = None
        try:
            series_ordinal = int(series_ordinal_qualifier_claim.getTarget())
        except:
            print("Series is not a number. Skipping!")
            continue
        parts[series_ordinal] = claim.getTarget()

    return dict(sorted(parts.items()))

def infer_series(item: pywikibot.ItemPage):
    item_dict = item.get()
    # print(f"Infering series of {pretty_label(item)}")

    if check_item_class_in_list(item, SERIES_CLASSES):
        # print(f"This item is a series.")
        return item

    item_series_list = item_dict["claims"]["P179"] # part of the series (P179)
    series: pywikibot.ItemPage = None

    for item_series_claim in item_series_list:
        series = item_series_claim.getTarget()
        # print(f"Found series {pretty_label(series)}.")

        if check_item_class_in_list(item, SERIES_CLASSES):
            # print("This item is validated as a series.")
            return series

        # print(clm_trgt)

    print("Can't infer series!")

def get_ref_from_item(item: pywikibot.ItemPage, reference_id_meta: ReferenceIDMeta) -> str | None:
    
    item_dict = item.get()
    if not reference_id_meta.property_id in item_dict['claims']:
        return None
    claims = item_dict['claims'][reference_id_meta.property_id]
    for claim in claims:
        claim: pywikibot.Claim
        return claim.getTarget()

# Generates a reference when the reference ID is set with consideration of the title in the external reference.
def generate_title_ref(based_on_ref_id: ReferenceID, subject_named_as: str, today_target: pywikibot.WbTime, ref_id_meta: ReferenceIDMeta, repo):
    claims = []

    stated_in_prop = pywikibot.Claim(repo, 'P248')                              # stated in
    stated_in_prop.setTarget(pywikibot.ItemPage(repo, ref_id_meta.stated_in_source_id))
    claims.append(stated_in_prop)

    based_on_heuristic_prop = pywikibot.Claim(repo, 'P887')                     # based on heuristic
    based_on_heuristic_prop.setTarget(pywikibot.ItemPage(repo, 'Q69652283'))    # inferred from title
    claims.append(based_on_heuristic_prop)

    ref_ref_id_prop = pywikibot.Claim(repo, ref_id_meta.property_id)
    ref_ref_id_prop.setTarget(str(based_on_ref_id))
    claims.append(ref_ref_id_prop)

    retrieved_prop = pywikibot.Claim(repo, 'P813')                              # retrieved
    retrieved_prop.setTarget(today_target)
    claims.append(retrieved_prop)

    if subject_named_as:
        subject_named_as_prop = pywikibot.Claim(repo, 'P1810')                  # subject named as
        subject_named_as_prop.setTarget(subject_named_as)
        claims.append(subject_named_as_prop)

    return claims

# Generates a reference when the reference ID is set from the external reference using the URL directly.
def generate_direct_url_ref(ref_id: ReferenceID, today_target: pywikibot.WbTime, ref_id_meta: ReferenceIDMeta, repo):
    stated_in_prop = pywikibot.Claim(repo, 'P248')                              # stated in
    stated_in_prop.setTarget(pywikibot.ItemPage(repo, ref_id_meta.stated_in_source_id))

    reference_url_prop = pywikibot.Claim(repo, 'P854')                          # reference URL
    reference_url_prop.setTarget(ref_id.get_ref())

    retrieved_prop = pywikibot.Claim(repo, 'P813')                              # retrieved
    retrieved_prop.setTarget(today_target)

    subject_named_as = ref_id.get_title_external()
    if subject_named_as:
        subject_named_as_prop = pywikibot.Claim(repo, 'P1810')                  # subject named as
        subject_named_as_prop.setTarget(subject_named_as)

    return stated_in_prop, reference_url_prop, retrieved_prop, subject_named_as_prop

# Generates a reference when the reference ID is derived from another item.
def generate_infer_ref(inferred_item: pywikibot.ItemPage, today_target: pywikibot.WbTime, ref_id_meta: ReferenceIDMeta, repo):
    inferred_from_prop = pywikibot.Claim(repo, 'P3452')                         # inferred from
    inferred_from_prop.setTarget(inferred_item)

    applies_to_prop = None
    if ref_id_meta.applies_to_source_id:
        applies_to_prop = pywikibot.Claim(repo, 'P518')                         # applies to
        applies_to_prop.setTarget(pywikibot.ItemPage(repo, ref_id_meta.applies_to_source_id))

    retrieved_prop = pywikibot.Claim(repo, 'P813')                              # retrieved
    retrieved_prop.setTarget(today_target)

    return_list = inferred_from_prop, applies_to_prop, retrieved_prop
    return_list = tuple(filter(lambda item: item is not None, return_list))

    return return_list

EPISODE_CLASSES = [
    "Q21191270",    # television series episode
    "Q1464125",     # web series episode
    "Q102364578",   # anime television series episode
    "Q116048824",   # animated series episode
    "Q29197",       # season premiere
    "Q653916",      # television pilot
    "Q1257934",     # clip show
    "Q2290276",     # season finale
    "Q3246768",     # Lesbian kiss episode
    "Q3252662",     # series finale
    "Q4949058",     # bottle episode
    "Q7601050",     # Star Trek crossover
    "Q7923105",     # very special episode
    "Q12311300",    # filler episode
    "Q13359539",    # Doctor Who (2013 specials)
    "Q20183934",    # backdoor pilot
    "Q21191265",    # Heartbeat episode
    "Q21664088",    # two-part episode
    "Q43178228",    # last episode
    "Q50062923",    # three-part episode
    "Q50914552",    # four-part episode
    "Q55422400",    # Sarah Jane Smith serial
    "Q57608327",    # radio series episode
    "Q61220733",    # Star Trek episode
    "Q61855877",    # podcast episode
    "Q72607030",    # Monster of the Week
    "Q79766755",    # Christmas episode
    "Q79768983",    # Easter episode
    "Q79769107",    # Halloween episode
    "Q79769844",    # New Year episode
    "Q79770458",    # Thanksgiving episode
    "Q79770638",    # Lucy episode
    "Q79848748",    # Columbus Day
    "Q79848760",    # Valentine's Day episode
    "Q90181054",    # video game episode
    "Q94970136",    # filler
    "Q97052294",    # episode of a webcomic
    "Q97054320",    # released episode of Pepper&Carrot
    "Q99079902",    # five-part episode
    "Q99296898",    # unproduced television episode
    "Q103842826",   # beach episode
    "Q104438898",   # lost television series episode
    "Q106082402",   # Baseball Episode
    "Q106797790",   # live podcast
    "Q106845483",   # anime episode partially based on a manga
    "Q106845592",   # anime episode based on a manga
    "Q106942341",   # talk show episode
    "Q109654109",   # non-explicit podcast episode
    "Q109677093",   # bonus podcast episode
    "Q109684855",   # teaser podcast episode
    "Q109750820",   # reposted podcast episode
    "Q110372546",   # Junior Eurovision Song Contest edition
    "Q110438165",   # podcast announcement episode
    "Q110730867",   # podcast bonus episode
    "Q110920134",   # Future Of House Radio episode
    "Q111660893",   # series premiere
    "Q114321689",   # morning show episode
    "Q120732784",   # web series pilot
    "Q123186929",   # six-part episode
    "Q123490564",   # lost podcast episode
    "Q124343615",   # mid-season finale
    "Q124662614",   # unaired television episode
    "Q125358813",   # unproduced animated television episode
    "Q125359117",   # animated two-part episode
    "Q125359123",   # animated three-part episode
    "Q125359129",   # animated four-part episode
    "Q125359137",   # animated five-part episode
    "Q125473326",   # radio drama episode
]

SEASON_CLASSES = [
    "Q3464665",     # television series season
    "Q61704031",    # web series season
    "Q100269041",   # anime television series season
    "Q29346471",    # season
    "Q69154911",    # podcast series season
    "Q70990126",    # creative work season
    "Q93992677",    # part of television season
    "Q107640824",   # unproduced television series season
    "Q115730787",   # radio series season
    "Q125354488",   # animated television season
    "Q125358951",   # unproduced animated television season
]

SERIES_CLASSES = [
    "Q5398426"      # television series
    "Q23739",       # soap opera
    "Q23745",       # telenovela
    "Q35769",       # WWE programs
    "Q170238",      # sitcom
    "Q178840",      # family television series
    "Q222639",      # swashbuckler film
    "Q278329",      # WWE Armageddon
    "Q338632",      # docu-soap
    "Q343782",      # action television series
    "Q399811",      # Japanese television drama
    "Q482612",      # Korean drama
    "Q526877",      # web series
    "Q542475",      # period drama film
    "Q662197",      # Večerníček
    "Q775344",      # teen drama
    "Q881912",      # Nordic Adult Christmas calendar
    "Q918098",      # British sitcom
    "Q1138628",     # asadora
    "Q1259759",     # miniseries
    "Q1273568",     # children's television series
    "Q1366112",     # drama television series
    "Q1566423",     # wheel series
    "Q1658957",     # Thai television soap opera
    "Q1676730",     # telecollege
    "Q1711400",     # youth series
    "Q1802588",     # Landesschau
    "Q2388283",     # téléroman
    "Q3080071",     # Brazilian telenovela
    "Q3951815",     # sceneggiato
    "Q4382232",     # procedural drama
    "Q4765080",     # animated sitcom
    "Q4783297",     # Arab television drama
    "Q4798414",     # drama 10
    "Q4922471",     # black sitcom
    "Q5219865",     # Danish television drama
    "Q5455086",     # fishing television series
    "Q6022825",     # Indian soap opera
    "Q7050677",     # Nordic Christmas calendar
    "Q7135559",     # paranormal reality television
    "Q7185299",     # Philippine drama
    "Q7437708",     # Q7437708
    "Q7724161",     # television serial
    "Q9335576",     # comedy television series
    "Q9335577",     # crime television series
    "Q10681387",    # Q10681387
    "Q11235226",    # yorudora (2000s)
    "Q11281236",    # yorudora
    "Q11323152",    # neo galaxy drama
    "Q11335932",    # premium drama
    "Q11396323",    # police television drama
    "Q11423603",    # Saturday drama (NHK)
    "Q11650048",    # galaxy drama
    "Q12242979",    # Syrian television series
    "Q12485565",    # Heonseondo
    "Q14509702",    # Hong Kong television drama
    "Q15977715",    # regional soap opera
    "Q16247289",    # Hispanic sitcom
    "Q16889492",    # limited-run series
    "Q17113138",    # teen sitcom
    "Q17145545",    # micro-series
    "Q19772367",    # Fawazeer Ramadan (Ramadan Riddles)
    "Q19952572",    # romantic television drama
    "Q20092645",    # Q20092645
    "Q20097097",    # Ukrainian television series
    "Q20267837",    # historical television series
    "Q21188110",    # American television sitcom
    "Q21190411",    # TV game show series
    "Q21191019",    # Japanese television series
    "Q21191068",    # Philippine television series
    "Q21232614",    # reality television series
    "Q21233490",    # erotic television series
    "Q21629439",    # youth television program
    "Q21664088",    # two-part episode
    "Q24855895",    # Web drama
    "Q24906243",    # Tamil television soap opera
    "Q25092487",    # Lucifer, season 1
    "Q27105351",    # Lucifer, season 2
    "Q27868077",    # documentary television series
    "Q28195059",    # Chinese television drama
    "Q34487266",    # telenarconovela
    "Q54932319",    # computer-animated television series
    "Q55082620",    # unproduced television series
    "Q55422400",    # Sarah Jane Smith serial
    "Q56064758",    # adventure television series
    "Q56320653",    # espionage television series
    "Q56878968",    # detective television series
    "Q62573441",    # Star Trek series
    "Q62903328",    # Floribella
    "Q63952888",    # anime television series
    "Q64224679",    # cancelled television series
    "Q64224805",    # cancelled animated television series
    "Q67175872",    # thriller television series
    "Q73505270",    # Teenage Mutant Ninja Turtles television series
    "Q74161894",    # Chinese TV series
    "Q76160317",    # Narcoserie
    "Q85133165",    # LGBTI+ related TV series
    "Q89273633",    # YouTube series
    "Q97240742",    # Q97240742
    "Q98526245",    # fantasy television series
    "Q100269041",   # anime television series season
    "Q100707163",   # television franchise
    "Q102227549",   # Chinese drama series
    "Q102245189",   # war television series
    "Q102430221",   # coming-of-age television program
    "Q104438889",   # lost television series
    "Q105971777",   # Japanese variety show
    "Q107042005",   # Chimudondon
    "Q108146417",   # Video Toon
    "Q108674843",   # non-existent television series
    "Q109981780",   # Instant Fiction
    "Q110311622",   # Q110311622
    "Q110720508",   # Q110720508
    "Q110940888",   # television film series
    "Q110955215",   # stop-motion animated television series
    "Q111241100",   # television series reboot
    "Q111241357",   # animated television series reboot
    "Q111747086",   # Handsome-Handsome Kucing Garong
    "Q111997258",   # action-adventure television series
    "Q112254601",   # alternate history television series
    "Q112977197",   # Q112977197
    "Q112981695",   # kolleg24
    "Q113480583",   # Scooby-Doo television series
    "Q113671041",   # original net animation series
    "Q115097963",   # anime-influenced animated television series
    "Q116515088",   # historical documentary television series
    "Q116546876",   # Mickey Mouse television series
    "Q117280527",   # Investigative television series
    "Q117467240",   # animated web series
    "Q117467246",   # animated television series
    "Q118752966",   # martial arts television series
    "Q118753160",   # knight television series
    "Q118753710",   # speculative/fantastic fiction television series
    "Q119346227",   # Cinderella is Online
    "Q121222277",   # Strannyy otpusk
    "Q122143112",   # live action/animated television series
    "Q122182536",   # Christmas television series
    "Q125122051",   # social issue television series
    "Q125359117",   # animated two-part episode
]

def compare_title_with_item(title: str, item: pywikibot.ItemPage):
    item_dict = item.get()

    if 'en' in item_dict['labels']:
        title_en_item = item_dict['labels']['en']

        is_title_match = lax_title_comparator(title_en_item, title)

        if is_title_match:
            return True

    # Extended match for all labels and aliases

    title_all_item = []
    title_all_item.extend(list(item_dict['labels'].values()))
    for aliases in list(item_dict['aliases'].values()):
        title_all_item.extend(aliases)
    return at_least_one_title_match(title_all_item, [title])