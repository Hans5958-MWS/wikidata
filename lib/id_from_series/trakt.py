import copy
import pywikibot

from config import TRAKT_API_KEY
from lib.id_from_series.classes import ReferenceID, ReferenceIDMeta
from lib.id_from_series.utils import compare_title_with_item, generate_infer_ref, generate_title_ref
import requests
import requests_cache

api_session = requests_cache.CachedSession('api')

class TraktID(ReferenceID):
    series_format = 'shows/{series}'
    season_format = 'shows/{series}/seasons/{season}'
    episode_format = 'shows/{series}/seasons/{season}/episodes/{episode}'
    series_ref_format = 'https://trakt.tv/shows/{series}'
    season_ref_format = 'https://trakt.tv/shows/{series}/seasons/{season}'
    episode_ref_format = 'https://trakt.tv/shows/{series}/seasons/{season}/episodes/{episode}'

    def is_valid_external(self, item: pywikibot.ItemPage):
        if not self.episode:
            return True

        title_external = self.get_title_external()
        
        if not title_external:
            return False
        
        if compare_title_with_item(title_external, item):
            return True
        
        return False

    def get_title_external(self):
        if not self.episode:
            return None

        season_response: requests.Response = api_session.get(f'https://api.trakt.tv/shows/{self.series}/seasons/{self.season}', headers={
            'Content-Type': 'application/json',
            'trakt-api-version': '2',
            'trakt-api-key': TRAKT_API_KEY
        })
        if not season_response.ok or not season_response.text:
            return None

        season_data = season_response.json()

        episode_data = None

        for data in season_data:
            if str(data['season']) != str(self.season) or str(data['number']) != str(self.episode):
                continue
            episode_data = data
            break

        # print(episode_data)

        if episode_data == None:
            return None

        return episode_data['title']
    
    def generate_claim_sources(self, data: dict, today_target: pywikibot.WbTime, repo):
        inferred_from: pywikibot.ItemPage = data.get("inferred_from", None)

        claim_sources = []
        
        if inferred_from:
            infer_ref = generate_infer_ref(inferred_from, today_target, TraktMeta, repo)
            claim_sources.append(infer_ref)

        if self.episode:
            title = self.get_title_external()
            title_ref_id = copy.deepcopy(self)
            title_ref_id.episode = None

            if title_ref_id:
                ref_id_prop = pywikibot.Claim(repo, TraktMeta.property_id)
                ref_id_prop.setTarget(str(title_ref_id))
                
                title_source = list(generate_title_ref(title_ref_id, title, today_target, TraktMeta, repo))
                claim_sources.append(title_source)

        return claim_sources

class TraktMeta(ReferenceIDMeta):
    name = "Trakt.tv ID"
    property_id = "P8013"                   # Property ID
    applies_to_source_id = "Q120493751"     # Trakt.tv ID
    stated_in_source_id = "Q84591894"       # trakt.tv
    ref_id_class = TraktID
