import copy
import pywikibot

from lib.id_from_series.classes import ReferenceID, ReferenceIDMeta
from lib.id_from_series.utils import compare_title_with_item, generate_infer_ref, generate_title_ref
import requests
import requests_cache
import requests
from bs4 import BeautifulSoup
import parse

api_session = requests_cache.CachedSession('api')

class MetacriticID(ReferenceID):
    series_format = 'tv/{series}'
    season_format = 'tv/{series}/season-{season}'
    episode_format = 'tv/{series}/season-{season}/episode-{episode}'
    series_ref_format = 'https://www.metacritic.com/' + series_format + '/'
    season_ref_format = 'https://www.metacritic.com/' + season_format + '/'
    episode_ref_format = 'https://www.metacritic.com/' + episode_format + '/'

    episode_from_ref_format = '{ordinal}-{extra}'

    def post_infill(self):
        if not self.episode:
            return
        
        episodes = get_episodes_in_season_external(self)

        if not episodes or self.episode not in episodes:
            return

        episode_data = episodes[self.episode]

        if episode_data and 'title' in episode_data:
            # print(episode_data['episode_on_ref'])
            self.episode_on_ref = episode_data['episode_on_ref']

    title_ref_source = None

    def get_title_ref(self):
        if self.title_ref_source == 'season':
            return self.get_season_ref()
        if self.title_ref_source == 'episode':
            return self.get_episode_ref()
        return None
    
    def get_title_ref_id(self):
        returned_ref_id = None
        if self.title_ref_source == 'season':
            returned_ref_id = copy.deepcopy(self)
            returned_ref_id.episode = None
        if self.title_ref_source == 'episode':
            returned_ref_id = copy.deepcopy(self)
        return returned_ref_id

    def is_valid_external(self, item: pywikibot.ItemPage):
        if not self.episode:
            return True

        title_external = self.get_title_external()

        if not title_external:
            return False
        
        if compare_title_with_item(title_external, item):
            return True
        
        if not self.episode_on_ref or self.episode_on_ref.endswith('episode-name-placeholder'):
            return False

    def get_title_external(self):
        print("get_title_external")
        if not self.episode:
            return None

        episodes = get_episodes_in_season_external(self)
        episode_data = episodes[self.episode]

        if episode_data and 'title' in episode_data:
            self.title_ref_source = "season"
            return episode_data['title']
        
        episode_data = get_episode_external(self)

        if episode_data and 'title' in episode_data:
            self.title_ref_source = "episode"
            return episode_data['title']
        
        return None, None
    
    def generate_claim_sources(self, data: dict, today_target: pywikibot.WbTime, repo):
        inferred_from: pywikibot.ItemPage = data.get("inferred_from", None)

        claim_sources = []
        
        if inferred_from:
            infer_source = generate_infer_ref(inferred_from, today_target, MetacriticMeta, repo)
            claim_sources.append(infer_source)

        if self.episode:
            title = self.get_title_external()
            title_ref_id = self.get_title_ref_id()

            if title_ref_id:
                ref_id_prop = pywikibot.Claim(repo, MetacriticMeta.property_id)
                ref_id_prop.setTarget(str(title_ref_id))
                
                title_source = list(generate_title_ref(title_ref_id, title, today_target, MetacriticMeta, repo))
                claim_sources.append(title_source)

        return claim_sources

class MetacriticMeta(ReferenceIDMeta):
    name = "Metacritic ID"
    property_id = "P1712"                   # Metacritic ID
    # applies_to_source_id = ""             # ???
    stated_in_source_id = "Q150248"         # Metacritic
    ref_id_class = MetacriticID
    use_title_compare = True

def get_episodes_in_season_external(ref_id: MetacriticID):

    response: requests.Response = api_session.get(ref_id.get_season_ref(), 
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0',
        }
    )
    
    if not response.ok:
        return None

    soup = BeautifulSoup(response.text, 'html.parser')

    episode_els = soup.select(".c-episodesModalCard a")
    # print(len(episode_els))

    episodes = {}

    for episode_els in episode_els:
        href = episode_els.get("href")
        title_el = episode_els.select_one(".c-episodesModalCard_title")
        episode_on_ref = parse.parse("/" + MetacriticID.episode_format + "/", href)['episode']
        episode = int(episode_on_ref.split('-')[0])
        episodes[episode] = {
            'href': href,
            'title': title_el.text.strip(),
            'episode_on_ref': episode_on_ref
        }


    return episodes

def get_episode_external(ref_id: MetacriticID):

    if not ref_id.episode:
        return None

    response: requests.Response = api_session.get(ref_id.get_episode_ref(), 
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0',
        }
    )

    if not response.ok:
        return None

    soup = BeautifulSoup(response.text, 'html.parser')

    title_el = soup.select_one(".c-productHero_seasonsComponent_episode")

    return {
        "title": title_el.text
    }
