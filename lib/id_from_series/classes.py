# episode = pywikibot.ItemPage(repo, "Q114543287")
# series = infer_series(pywikibot.ItemPage(repo, "Q114543287"))

import copy
import parse
import pywikibot

class ReferenceID:
    series_format = ''
    season_format = ''
    episode_format = ''

    series_ref_format = ''
    season_ref_format = ''
    episode_ref_format = ''

    episode_from_ref_format = ''
    season_from_ref_format = ''

    # Numerical ID
    series: str = None
    season: int = None
    episode: int = None

    # Real ID on reference
    season_on_ref: str = None
    episode_on_ref: str = None

    def __init__(self, str_id):
        if (str_id):
            self.parse(str_id)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__str__() == other.__str__()
        else:
            return False
        
    def __ne__(self, other):
        return not self.__eq__(other)

    def parse(self, str_id):
        self.parse_from_episode(str_id)
        if self.episode:
            return
        self.parse_from_season(str_id)
        if self.season:
            return
        self.parse_from_series(str_id)

    def __str__(self):
        return self.format() or ''

    def format(self):
        if self.series:
            if self.season:
                if self.episode:
                    return self.format_episode()
                return self.format_season()
            return self.format_series()

    def get_ref(self):
        if self.series:
            if self.season:
                if self.episode:
                    return self.get_episode_ref()
                return self.get_season_ref()
            return self.get_series_ref()
        return None

    def parse_from_series(self, str_id):
        result = parse.parse(self.series_format, str_id)
        if result:
            self.series = result['series']

    def parse_from_season(self, str_id):
        result = parse.parse(self.season_format, str_id)
        
        if not result:
            return
        
        self.series = result['series']
        
        if self.season_from_ref_format:
            result_2 = parse.parse(self.season_from_ref_format, result['season'])
            if result_2:
                self.season = int(result_2['ordinal'])
            self.season_on_ref = result['season']
        else:
            self.season = result['season']

    def parse_from_episode(self, str_id):
        result = parse.parse(self.episode_format, str_id)
        
        if not result:
            return
        
        self.series = result['series']
        
        if self.season_from_ref_format:
            result_2 = parse.parse(self.season_from_ref_format, result['season'])
            if result_2:
                self.season = int(result_2['ordinal'])
            self.season_on_ref = result['season']
        else:
            self.season = result['season']
        
        if self.episode_from_ref_format:
            result_2 = parse.parse(self.episode_from_ref_format, result['episode'])
            if result_2:
                self.episode = int(result_2['ordinal'])
            self.episode_on_ref = result['episode']
        else:
            self.episode = result['episode']

    def format_series(self):
        if self.series:
            return self.series_format.format(series=self.series)

    def format_season(self):
        if self.series and self.season:
            return self.season_format.format(series=self.series, season=self.season_on_ref or self.season)

    def format_episode(self):
        if self.series and self.season and self.episode:
            return self.episode_format.format(series=self.series, season=self.season_on_ref or self.season, episode=self.episode_on_ref or self.episode)

    def get_series_ref(self):
        if self.series:
            return self.series_ref_format.format(series=self.series)

    def get_season_ref(self):
        if self.series and self.season:
            return self.season_ref_format.format(series=self.series, season=self.season_on_ref or self.season)

    def get_episode_ref(self):
        if self.series and self.season and self.episode:
            return self.episode_ref_format.format(series=self.series, season=self.season_on_ref or self.season, episode=self.episode_on_ref or self.episode)

    def tuple(self):
        return self.series, self.season, self.episode

    def is_valid_format(self):

        if not (self.series or (self.season and self.series) or (self.episode and self.season and self.series)):
            return False
        
        if self.season and self.season_from_ref_format:
            try:
                result_2 = parse.parse(self.season_from_ref_format, self.season)
                if not result_2:
                    return False
                if self.season == int(result_2['ordinal']):
                    return False
            except:
                return False
            
        if self.episode and self.episode_from_ref_format:
            try:
                result_2 = parse.parse(self.episode_from_ref_format, self.episode)
                if not result_2:
                    return False
                if self.episode == int(result_2['ordinal']):
                    return False
            except:
                return False

        return True
    
    def post_infill(self):
        # Abstract. To implement.
        return
    
    def pre_infill(self):
        # Abstract. To implement.
        return

    def is_valid_external(self, item: pywikibot.ItemPage):
        # Abstract: True by default. To implement.
        return True
    
    def get_title_external(self):
        # Abstract: None by default. To implement.
        return None
    
    def generate_claim_sources(self, data: dict, today_target: pywikibot.WbTime, repo):
        # Abstract: None by default. To implement.
        return []

class ReferenceIDMeta:
    name = ""
    property_id = ""
    applies_to_source_id = ""
    stated_in_source_id = ""
    ref_id_class: ReferenceID = None
    use_title_compare = False
    use_direct_url_match = False

class ItemMatch:
    item: pywikibot.ItemPage = None
    reference_id: ReferenceID = None
    reference_id_orig: ReferenceID = None
    inferred_from: pywikibot.ItemPage = None

    def __init__(self, item: pywikibot.ItemPage, reference_id: ReferenceID):
        self.item = item
        self.reference_id = reference_id
        self.reference_id_orig = copy.deepcopy(reference_id)

class EpisodeMatch(ItemMatch):
    is_matched_by_title = False
    is_matched_by_parent_threshold = False

class SeasonMatch(ItemMatch):
    is_matched_by_threshold = False
    episode_count: int = 0
    episode_match_count: int = 0


