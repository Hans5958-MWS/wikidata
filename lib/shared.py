def generate_summary(task, message, test_id=None, link_on_task=True):
    prefix = '🤖 '
    if link_on_task:
        prefix += f"[[User:Bot5958/T/{task}|Task {task}]]"
    else:
        prefix += f"Task {task}"
    if test_id:
        prefix += f" (test id: {test_id} · [[:toolforge:editgroups/b/CB/{test_id}|details]])"
    return f'{prefix}: {message}'