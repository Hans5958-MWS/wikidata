month_dict = {
	"01": "Januari",
	"02": "Februari",
	"03": "Maret",
	"04": "April",
	"05": "Mei",
	"06": "Juni",
	"07": "Juli",
	"08": "Agustus",
	"09": "September",
	"10": "Oktober",
	"11": "November",
	"12": "Desember",
}

def fast_date_conv(date_str):
	[ year, month, day ] = date_str.split('-')
	month = month_dict[month]
	if day[0] == '0':
		day = day[1:]
	return f"{day} {month} {year}"

res_file_lines = 0
res_file_qty = 1
res_file = open(f"all-dates/qs/Indonesian date labels (part {res_file_qty} at 10k).tsv", 'w')

with open('all-dates/all-dates.tsv') as f:
	f.readline()
	for line in f.readlines():
		[ item_link, date_raw ] = line.split('\t')
		date = date_raw.split('T')[0]
		if not date or '-' not in date:
			continue
		date_conv = fast_date_conv(date)
		item = item_link.split('entity/')[1]
		# print(date_conv)
		res_file.write("\t".join([
			item,
			"Lid",
			f'"{date_conv}"'
		]))
		res_file.write('\n')
		res_file_lines += 1

		if res_file_lines == 10000:
			print("New")
			res_file_lines = 0
			res_file_qty += 1
			res_file = open(f"all-dates/qs/Indonesian date labels (part {res_file_qty} at 10k).tsv", 'w')