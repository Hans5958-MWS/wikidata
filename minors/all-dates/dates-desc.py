month_dict = {
	"01": "Januari",
	"02": "Februari",
	"03": "Maret",
	"04": "April",
	"05": "Mei",
	"06": "Juni",
	"07": "Juli",
	"08": "Agustus",
	"09": "September",
	"10": "Oktober",
	"11": "November",
	"12": "Desember",
}

def desc_gen(date_str):
	try:
		[ year, month, day ] = date_str.split('-')
		if int(year) < 1800:
			return "\"tanggal di Kalender Gregorius\""
		return "\"tanggal\""
	except:
		return "\"tanggal\""

res_file_lines = 0
res_file_qty = 1
res_file = open(f"all-dates/qs/Indonesian date description (part {res_file_qty}).tsv", 'w')

with open('all-dates/all-dates.tsv') as f:
	f.readline()
	for line in f.readlines():
		[ item_link, date_raw ] = line.split('\t')
		item = item_link.split('entity/')[1]
		res_file.write("\t".join([
			item,
			"Did",
			desc_gen(date_raw)
		]))
		res_file.write('\n')
		res_file_lines += 1

		if res_file_lines == 15000:
			print("New")
			res_file_lines = 0
			res_file_qty += 1
			res_file = open(f"all-dates/qs/Indonesian date description (part {res_file_qty}).tsv", 'w')