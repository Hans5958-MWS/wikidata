from datetime import date
import json
import requests
from bs4 import BeautifulSoup
import phonenumbers
from lib import data_type_to_wikidata_property

org_data_list = []

with open("sipp/kementrian.json") as f:
	org_data_list = json.loads(f.read())

for org_data in org_data_list:
	# print('/*', org_data['name'], org_data['wikidata_item'], '*/')
	for data_type in org_data['data']:
		property = ""
		if data_type in data_type_to_wikidata_property:
			property = data_type_to_wikidata_property[data_type]

		# if data_type in org_data['data'] and str is type(org_data['data'][data_type]):
		# 	org_data['data'][data_type] = [org_data['data'][data_type]]
		# 	org_data['data_source'][data_type] = [[org_data['data_source'][data_type]]]
		# 	org_data['data_date'][data_type] = [[org_data['data_date'][data_type]]]

		for i, content in enumerate(org_data['data'][data_type]):

			if data_type == "coords":
				content = f"@{'/'.join(content.split(','))}"
			elif data_type == "email":
				content = f'"mailto:{content}"'
			elif data_type == "site":
				content = f'"{content}"\tP407\tQ9240'
				# P407: language of work or name
			else:
				content = f'"{content}"'

			for j, source in enumerate(org_data['data_source'][data_type][i]):
				date = org_data['data_date'][data_type][i][j]
				command = '\t'.join([
					org_data["wikidata_item"],
					property,
					content,
					"S854",
					source,
					"S813",
					f"+{date}T00:00:00Z/11"
				])
				print(command)
