from datetime import date
import json
import requests
from bs4 import BeautifulSoup
import phonenumbers

org_data_list = []

with open("sipp/kementrian.txt.json") as f:
	org_data_list = json.loads(f.read())

for org_data in org_data_list:
	if not 'phone' in org_data['data']:
		continue

	x = phonenumbers.parse(org_data['data']['phone'], "ID")

	org_data['data']['phone'] = phonenumbers.format_number(x, phonenumbers.PhoneNumberFormat.RFC3966).split(':')[1]

with open("sipp/kementrian2.json", 'w') as f:
	f.write(json.dumps(org_data_list))