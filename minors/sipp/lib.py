from datetime import date

def add_data_to_array(data_array, value_new, source_url_new, date_new = date.today()):

	date_new = f"+{date_new.isoformat()}T00:00:00Z/11"

	for data in data_array:
		if not data['value'] == value_new:
			continue

		for source in data['source']:
			if source['url'] in source_url_new:
				return
		data['source'].append({
			'url': source_url_new,
			'date': date_new
		})
		return
	
	data_array.append({
		'value': value_new,
		'source': [{
			'url': source_url_new,
			'date': date_new
		}]
	})

def add_org_data(org_data, data_type, value_new, source_url_new, date_new = date.today()):
	if data_type not in org_data['data']:
		org_data['data'][data_type] = []

	add_data_to_array(org_data['data'][data_type], value_new, source_url_new, date_new)

class IdentifierRegexPatterns:

	TWITTER = r"^https?://(?:www\.)?twitter\.com/([A-Za-z0-9_]{1,15})/?$"
	YOUTUBE = r"^https?://(?:www\.)?youtube\.com/channel/(UC([A-Za-z0-9_\-]){22})/?$"
	YOUTUBE_HANDLE = r"^https:\/\/www\.youtube\.com\/@([A-Za-z0-9_\-\.]{3,30})"
	INSTAGRAM = r"^https?://(?:www\.)?instagram\.com/([a-z0-9_\.]+)/?$"
	FACEBOOK = r"^https?://(?:www\.)?(?:fb|facebook)\.com/([\w\d.-]+)/?$"
	FACEBOOK_PAGE = r"^https?://(?:www\.)?(?:fb|facebook)\.com/pages/([\w\d.-]+/[1-9][0-9]+)/?(\?f?ref=[a-z_]+)?(\?sk=info&tab=page_info)?$"
	TIKTOK = r"^https?:\/\/(?:www\.)?tiktok\.com\/@((?!.*\.\.)(?!.*\.$)[^\W][\w.]{2,24})"
	THREADS = r"^https?:\/\/(?:www\.)?threads\.net\/@(\_{0,2}[a-zA-Z\d]+(?:(?:\.|\_{1,4}|\.\_)[a-zA-Z\d]+)*\_{0,2})"
	LINKEDIN = r"^https?:\/\/(?:www\.)?linkedin\.com\/(?:company|school|showcase)\/([^\/?#]+)"

ALL_IDENTIFIER_REGEX_PATTERNS = [
	IdentifierRegexPatterns.TWITTER, 
	IdentifierRegexPatterns.YOUTUBE, 
	IdentifierRegexPatterns.YOUTUBE_HANDLE, 
	IdentifierRegexPatterns.INSTAGRAM, 
	IdentifierRegexPatterns.FACEBOOK, 
	IdentifierRegexPatterns.FACEBOOK_PAGE, 
	IdentifierRegexPatterns.TIKTOK, 
	IdentifierRegexPatterns.THREADS
]

data_type_to_regex = {
	'twitter': IdentifierRegexPatterns.TWITTER, 
	'youtube': IdentifierRegexPatterns.YOUTUBE, 
	'youtube_handle': IdentifierRegexPatterns.YOUTUBE_HANDLE, 
	'instagram': IdentifierRegexPatterns.INSTAGRAM, 
	'facebook': IdentifierRegexPatterns.FACEBOOK, 
	'facebook_page': IdentifierRegexPatterns.FACEBOOK_PAGE, 
	'tiktok': IdentifierRegexPatterns.TIKTOK, 
	'threads': IdentifierRegexPatterns.THREADS
}

data_type_to_wikidata_property = {
	'phone': "P1329",			# phone number
	'email': "P968",			# email address
	'site': "P856",
	'coords': "P625",
	'twitter': "P2002",
	'youtube': "P2397",
	'youtube_handle': "P11245",	# YouTube handle
	'instagram': "P2003",		# Instagram username
	'facebook': "P2013",		# Facebook username
	'facebook_page': "P4003",	# Facebook page ID
	'tiktok': "P7085",			# TikTok username
	'threads': "P11892",		# Threads username
	'linkedin': "P4264",		# LinkedIn company or organization ID
}