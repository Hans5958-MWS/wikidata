import json
import re
from lib import data_type_to_wikidata_property

org_data_list = []

def add_org_data(org_data, data_type, data_raw, content_new):
	(content, source, date) = data_raw
	if content_new:
		content = content_new
	org_data['data'][data_type] = content
	org_data['data_source'][data_type] = source
	org_data['data_date'][data_type] = date


with open("sipp/kementrian.json") as f:
	org_data_list = json.loads(f.read())

for org_data in org_data_list:
	data_raws = org_data['data_raws']
	data_raws_new = []

	if not data_raws:
		continue

	for i, data_raw in enumerate(data_raws):
		(content, source, date) = data_raw

		got_match = False

		for (data_type, regex) in enumerate(data_type_to_wikidata_property):
			match = re.match(regex, content)
			if match:
				add_org_data(org_data, data_type, match.group(1))
				got_match = True
				break

		if not got_match:
			data_raws_new.append(data_raw)

	data_raws.clear()
	data_raws.extend(data_raws_new)

with open("sipp/kementrian2.json", 'w') as f:
	f.write(json.dumps(org_data_list))