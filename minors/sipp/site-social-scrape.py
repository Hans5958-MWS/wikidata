from datetime import date
import json
import requests
from bs4 import BeautifulSoup
import re

from lib import ALL_IDENTIFIER_REGEX_PATTERNS, IdentifierRegexPatterns, add_org_data, data_type_to_wikidata_property

org_data_list = []

with open("sipp/kementrian.json") as f:
	org_data_list = json.loads(f.read())

for org_data in org_data_list:
	if not 'site' in org_data['data']:
		continue

	print(org_data['name'])
	site = org_data['data']['site']

	# try:
	# 	response = requests.get(site, timeout=1)
	# except requests.exceptions.SSLError as e:
	# 	response = requests.get(site, verify=False)
	try:
		response = requests.get(site, verify=False, timeout=5)
	except Exception as e:
		continue
	soup = BeautifulSoup(response.text, 'html.parser')

	a_els = soup.select("a")
	for el in a_els:
		href = el.get('href')
		if not href:
			continue
		
		for (data_type, regex) in enumerate(data_type_to_wikidata_property):
			match = re.match(regex, href)
			if match:
				add_org_data(org_data, data_type, match.group(1))
				break


with open("sipp/kementrian2.json", 'w') as f:
	f.write(json.dumps(org_data_list))