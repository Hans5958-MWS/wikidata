from datetime import date
import json
import requests
from bs4 import BeautifulSoup
from lib import add_org_data, add_data_to_array

source_url_list = []

with open("sipp/lembaga.txt") as f:
	lines = f.readlines()
	for line in lines:
		source_url_list.append(line.strip())

# print(site_list)

org_data_list = []


for source_url in source_url_list:

	org_data = {
		'name': '',
		'wikidata_item': '',
		'data': {},
		'_raws' : []
	}

	print(source_url)

	response = requests.get(source_url)
	soup = BeautifulSoup(response.text, 'html.parser')

	name_el = soup.select_one(".card h4")
	if name_el:
		org_data['name'] = name_el.get_text()
		print(org_data['name'])

	phone_el = soup.select_one("div.institution-contact:nth-child(1) > a:nth-child(2)")
	if phone_el and 'tel:' in phone_el.get('href'):
		add_org_data(org_data, 'phone', phone_el.get('href').split('tel:')[1], source_url)

	email_el = soup.select_one("div.institution-contact:nth-child(3) > a:nth-child(2)")
	if email_el and 'to:' in email_el.get('href'):
		add_org_data(org_data, 'email', email_el.get('href').split('to:')[1], source_url)

	site_el = soup.select_one("div.institution-contact:nth-child(5) > a:nth-child(2)")
	if site_el:
		add_org_data(org_data, 'site', site_el.get('href'), source_url)

	maps_el = soup.select_one("a.btn:nth-child(1)")
	if maps_el:
		add_org_data(org_data, 'coords', maps_el.get('href').split('?q=')[1], source_url)

	socmed_els = soup.select(".socmed-link a")
	for el in socmed_els:
		add_data_to_array(org_data['_raws'], el.get('href'), source_url)

	print(org_data)

	# exit()
	org_data_list.append(org_data)

	with open("sipp/lembaga.json", 'w') as f:
		f.write(json.dumps(org_data_list))