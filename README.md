# [MWS](https://gitlab.com/Hans5958-MWS) - Wikidata

This is the collection of scripts I use for editing in [wikidata.org](https://wikidata.org).

## Contents

| Name | Type | Description | Reference |
| - | - | - | - |
| id_from_series | Pywikibot script | Infer reference IDs of TV series, seasons, and episods from vertically adjacent levels. | [1](https://www.wikidata.org/wiki/Wikidata:Requests_for_permissions/Bot/Bot5958_1) |
| minors | Various | Collection of minor scripts. | |

TODO

## License

MIT